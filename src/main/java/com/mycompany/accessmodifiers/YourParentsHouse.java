/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.accessmodifiers;

/**
 *
 * @author nunezd
 */
public class YourParentsHouse
{
  protected boolean enterYourParentsHouse ()
  {
    if (Math.random() >= 0.5)
    {
      System.out.println("Entering YourParentsHouse");
      return true;
    }
    else
    {
      System.out.println("No one is home at YourParentsHouse");
      return false;
    }
  }
 
  private void changeThermostat ()
  {
    System.out.println("Only your parents can change their thermostat!");
  }
}