/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.accessmodifiers;



/**
 *
 * @author nunezd
 */
public class YourHouse extends YourParentsHouse {

    //YourHouse extends YourParentsHouse which means that YourHouse will have
    //access to all of the public and protected methods inside YourParentsHouse, 
    //as well as all of the public, protected and private methods within its own
    //code
    protected void enterYourHouse() {
        {
            System.out.println("Entering " + getClass().getSimpleName());
        }
    }
    public void knockOnDoor() {
        System.out.println("You knock on the door of " + getClass().getSimpleName());
        double randomNumber = Math.random();
        if (randomNumber >= 0.5) {
            System.out.println("You are greeted at " + getClass().getSimpleName());
            enterYourHouse();
        } else {
            System.out.println("No one answers");
            goToParentsHouse();
        }
    }

    protected void goToParentsHouse() {
        System.out.println("You are going to YourParentsHouse");
        if (!this.enterYourParentsHouse()) {
            YourNeighboursHouse yourNeighboursHouse = new YourNeighboursHouse();
            yourNeighboursHouse.enterYourNeighboursHouse();
        }
    }
}
