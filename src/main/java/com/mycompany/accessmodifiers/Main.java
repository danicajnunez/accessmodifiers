/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author nunezd
 */
package com.mycompany.accessmodifiers;

/**
 *
 * @author nunezd
 * This is the main method for this project
 */
public class Main
{

    /**
     *
     * This is the main class for this project
     * @param args
     */
    
    public static void main (String[] args)
  {
    YourHouse yourHouse = new YourHouse();
    yourHouse.knockOnDoor();
  }
}
