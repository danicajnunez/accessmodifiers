/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.accessmodifiers;

/**
 *
 * @author nunezd
 */
class YourNeighboursHouse
{
  void enterYourNeighboursHouse ()
  {
    System.out.println("Your neighbour is home, so you enter " + getClass().getSimpleName());
  }
}